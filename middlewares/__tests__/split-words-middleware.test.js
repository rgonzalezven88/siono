const middleware = require("./../split-words-middleware");
const fs = require("fs");

describe("split-words-middleware", () => {
    const res = {}
    it("next is called without args", () => {
        const nextFunc = jest.fn();
        const req = {
            file: {
                buffer: new Buffer('hello world')
            }
        }
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(req.words).toEqual(['hello', 'world']);
    });
});
const supertest = require('supertest');
const app = require("./../../app");

describe("file routes", () => {
    it("POST /file", async () => {
        await supertest(app)
            .post("/file")
            .field('n', 2)
            .attach('file', `${__dirname}/test.txt`)
            .expect(200)
            .then((response) => {
                expect(response.status).toEqual(200);
                expect(response.body).toEqual({
                    frequencies: [{ word: 'sed', count: 12 }, { word: 'id', count: 10 }]
                });
            })
    });
});

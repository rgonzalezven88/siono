var { REGEXP_EXTENDED_ASCII, REGEXP_LATIN_WORD, REGEXP_WORD } = require("./../constants/regex");

var words = function (subject) {
    var patternRegExp = REGEXP_EXTENDED_ASCII.test(subject) ? REGEXP_LATIN_WORD : REGEXP_WORD;
    return subject.match(patternRegExp);
}

var countDuplicated = function (word, array) {
    return array.filter((wordMapped) => wordMapped === word).length;
}

var getUniques = function (array) {
    return Array.from(new Set(array));
}

module.exports = {
    words,
    countDuplicated,
    getUniques
}

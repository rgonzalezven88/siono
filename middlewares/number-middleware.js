var { getUniques } = require("./../utils/strings");

module.exports = function (req, res, next) {
    const { n } = req.body;
    if (!n) {
        const error = new Error('Param n is required');
        error.status = 400;
        next(error);
    } else if (n < 1) {
        const error = new Error('Param n is invalid');
        error.status = 400;
        next(error);
    } else if (n > getUniques(req.words).length) {
        const error = new Error('Param n is invalid');
        error.status = 400;
        next(error);
    } else {
        next();
    }

}
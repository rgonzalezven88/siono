module.exports = function (req, res, next) {
    const file = req.file;
    if (!file) {
        const error = new Error("Please upload a file");
        error.status = 400;
        return next(error);
    } else {
        next();
    }
}
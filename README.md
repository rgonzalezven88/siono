# Siono Code Test

## Reqs 📋

_Node / Npm_

## Install dependencies 🔧

```
npm install
```

## Start 🚀

```
npm start
```

## Tests ⚙️

```
npm run test
```

_You can test with a curl when the app is running with npm start_

```
curl --location --request POST 'http://localhost:3000/file' \
--form 'file=@"./public/test.txt"' \
--form 'n="5"'
```

_You will get this response_

```
{"frequencies":[{"word":"sed","count":12},{"word":"id","count":10},{"word":"sit","count":8},{"word":"amet","count":8},{"word":"ut","count":8}]}
```

## Author ✒️

* **Roberto Gonzalez** - *Developer* - [rogonzalez](https://github.com/rogonzalez88)
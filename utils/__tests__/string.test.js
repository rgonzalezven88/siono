const strings = require("./../strings");

describe("strings lib", () => {
    it("returns uniques values from array", () => {
        const array = ["a", "b", "c", "a", "b"];
        expect(strings.getUniques(array)).toEqual(["a", "b", "c"]);
    });
    it("returns count of duplicated values", () => {
        const array = ["a", "b", "c", "a", "b"];
        expect(strings.countDuplicated("a", array)).toEqual(2);
    });
    it("returns words from text", () => {
        const text = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
        
        Integer id purus imperdiet, condimentum enim a, volutpat nisi.`;
        expect(strings.words(text)).toEqual([
            "Lorem",
            "ipsum",
            "dolor",
            "sit",
            "amet",
            "consectetur",
            "adipiscing",
            "elit",
            "Integer",
            "id",
            "purus",
            "imperdiet",
            "condimentum",
            "enim",
            "a",
            "volutpat",
            "nisi"
        ]);
    });
});
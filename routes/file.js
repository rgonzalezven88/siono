var express = require('express');
var multer = require("multer");
var numberMiddleware = require("./../middlewares/number-middleware");
var fileMiddleware = require("./../middlewares/file-middleware");
var splitWordsMiddleware = require("./../middlewares/split-words-middleware");

var { getUniques, countDuplicated } = require("./../utils/strings");

var router = express.Router();

var storage = multer.memoryStorage();
var upload = multer({
  storage: storage,
  // TODO: Move to constants
  limits: { fileSize: 1024 * 1024 * 1024 },
  // TODO: Move to utils
  fileFilter: (req, file, cb) => {
    if (file.mimetype !== 'text/plain') {
      const error = new Error('file is not allowed')
      error.status = 400;
      return cb(error);
    }

    cb(null, true)
  }
});

/* Upload file */
router.post('/', upload.single("file"), fileMiddleware, splitWordsMiddleware, numberMiddleware, function (req, res, next) {
  // TODO: Move to a controller
  const uniques = getUniques(req.words);
  const frequencies = uniques.map(word => ({
    word,
    count: countDuplicated(word, req.words)
  })).sort((a, b) => b.count - a.count)
    .slice(0, req.body.n);
  res.send({
    frequencies
  });
});

module.exports = router;

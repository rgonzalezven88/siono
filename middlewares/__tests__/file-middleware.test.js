const middleware = require("./../file-middleware");

describe("file-middleware", () => {
    const nextFunc = jest.fn();
    const res = {}
    it("next is called without args", () => {
        const req = {
            file: {
                size: 123566
            }
        }
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith();
    });
    it("next is called with error", () => {
        const req = {}
        const error = new Error("Please upload a file");
        error.status = 400;
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith(error);
    });
});
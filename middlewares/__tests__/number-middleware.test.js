const middleware = require("./../number-middleware");

describe("number-middleware", () => {
    const res = {}
    it("next is called without args", () => {
        const nextFunc = jest.fn();
        const req = {
            words: ["a", "b", "c"],
            body: {
                n: 2
            }
        }
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith();
    });
    it("next is called with error: 'Param n is required'", () => {
        const nextFunc = jest.fn();
        const req = {
            body: {}
        }
        const error = new Error("Param n is required");
        error.status = 400;
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith(error);
    });
    it("next is called with error: 'Param n is invalid'", () => {
        const nextFunc = jest.fn();
        const req = {
            body: {
                n: -2
            }
        }
        const error = new Error("Param n is invalid");
        error.status = 400;
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith(error);
    });
    it("next is called with error: 'Param n is invalid'", () => {
        const nextFunc = jest.fn();
        const req = {
            words: ["a", "b", "c"],
            body: {
                n: 4
            }
        }
        const error = new Error("Param n is invalid");
        error.status = 400;
        middleware(req, res, nextFunc);
        expect(nextFunc).toHaveBeenCalled();
        expect(nextFunc).toHaveBeenCalledWith(error);
    });
});
var { words } = require("../utils/strings");

module.exports = function (req, res, next) {
    const file = req.file;
    const result = Buffer.from(file.buffer).toString("utf-8");
    req.words = words(result).map(word => word.toLowerCase());
    next();
}